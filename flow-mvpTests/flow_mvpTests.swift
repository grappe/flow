//
//  flow_mvpTests.swift
//  flow-mvpTests
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import XCTest
@testable import flow_mvp

class flow_mvpTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testData() throws {
        
        let data1 = FlowModel(timestamp: 1594586340, voc: 51.525246, no2: 52.18937, pm10: 49.205154, pm25: 41.99668)
        let data2 = FlowModel(timestamp: 1594586540, voc: 81.525246, no2: 22.18937, pm10: 149.205154, pm25: 71.99668)

        
        XCTAssertEqual(data1.getMaxValue(), Float(52.18937))
        XCTAssertEqual(data1.getColorFrom(pollutant: data1.voc), UIColor.systemRed)
        XCTAssertEqual(data2.getMaxValue(), Float(149.205154))
        XCTAssertEqual(data1.getColorFrom(pollutant: data2.pm10), UIColor.systemPurple)
    }
}
