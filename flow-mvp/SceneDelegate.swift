//
//  SceneDelegate.swift
//  flow-mvp
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import UIKit


@available(iOS 13.0, *)

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
       var window: UIWindow?

}


@available(iOS 13.0, *)
func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

}
