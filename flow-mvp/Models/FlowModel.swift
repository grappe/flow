//
//  FlowModel.swift
//  flow-mvp
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import Foundation
import UIKit

enum Pollutant {
    case voc
    case no2
    case pm10
    case pm25
}

struct FlowModel: Codable {
    let timestamp: Int
    var voc: Float
    var no2: Float
    var pm10: Float
    var pm25: Float
    
    func getDateString() -> String {
        let date = Date(timeIntervalSince1970: Double(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    func getMaxValue() -> Float {
        var max = voc
        max = no2 > max ? no2 : max
        max = pm10 > max ? pm10 : max
        max = pm25 > max ? pm25 : max

        return max
    }
    
    func getColorFrom(pollutant: Float) -> UIColor {
        switch pollutant {
        case 0..<21:
            return .systemGreen
        case 21..<51:
            return .systemYellow
        case 51..<101:
            return .systemRed
        default:
            return .systemPurple
        }
    }
}
