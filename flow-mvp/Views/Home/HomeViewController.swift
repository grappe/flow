//
//  ViewController.swift
//  flow-mvp
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import UIKit
import Bluetooth

class HomeViewController: UIViewController {
    
    var presenter: HomePresenter?
    @IBOutlet weak var connectedLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var vocLabel: UILabel!
    @IBOutlet weak var vocBarView: UIView!
    @IBOutlet weak var vocWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var no2Label: UILabel!
    @IBOutlet weak var no2BarView: UIView!
    @IBOutlet weak var no2WidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pm10Label: UILabel!
    @IBOutlet weak var pm10BarView: UIView!
    @IBOutlet weak var pm10WidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pm25Label: UILabel!
    @IBOutlet weak var pm25BarView: UIView!
    @IBOutlet weak var pm25WidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var seeOlder: UIButton!
    @IBOutlet weak var seeMoreRecent: UIButton!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    
    @IBOutlet weak var showLiveLabel: UILabel!
    @IBOutlet weak var showLiveIndicatorView: UIView!
    @IBOutlet weak var timeIndicator: UIView!
    @IBOutlet weak var timeIndicatorHeight: NSLayoutConstraint!
    @IBOutlet weak var timeIndicatorRightMargin: NSLayoutConstraint!


    var currentIndex = 0
    var oldPanOffset = 0.0 as CGFloat
    var flowDataSize = 0
    var showLive = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = HomePresenter(delegate: self)
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        swipeView.addGestureRecognizer(gestureRecognizer)
        
        let path = UIBezierPath(roundedRect:vocBarView.bounds,
                                byRoundingCorners:[.topRight, .bottomRight],
                                cornerRadii: CGSize(width: Constants.UIValues.cornerRadius,
                                                    height: Constants.UIValues.cornerRadius))

        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        
        vocLabel.backgroundColor = UIColor.init(white: Constants.UIValues.whiteLabel,
                                                alpha: Constants.UIValues.alphaLabel)
        vocLabel.layer.cornerRadius = Constants.UIValues.cornerRadius
        vocLabel.clipsToBounds = true
        setupRightCornersRadii(view: vocBarView)

        
        no2Label.backgroundColor = UIColor.init(white: Constants.UIValues.whiteLabel,
                                                alpha: Constants.UIValues.alphaLabel)
        no2Label.layer.cornerRadius = Constants.UIValues.cornerRadius
        no2Label.clipsToBounds = true
        setupRightCornersRadii(view: no2BarView)
        
        pm10Label.backgroundColor = UIColor.init(white: Constants.UIValues.whiteLabel,
                                                 alpha: Constants.UIValues.alphaLabel)
        pm10Label.layer.cornerRadius = Constants.UIValues.cornerRadius
        pm10Label.clipsToBounds = true
        setupRightCornersRadii(view: pm10BarView)
        
        pm25Label.backgroundColor = UIColor.init(white: Constants.UIValues.whiteLabel,
                                                 alpha: Constants.UIValues.alphaLabel)
        pm25Label.layer.cornerRadius = Constants.UIValues.cornerRadius
        pm25Label.clipsToBounds = true
        setupRightCornersRadii(view: pm25BarView)
        
        timeIndicator.layer.cornerRadius = timeIndicator.frame.size.width / 2
        timeIndicator.clipsToBounds = true
        
        
        showLiveIndicatorView.layer.cornerRadius = showLiveIndicatorView.frame.size.width / 2
        showLiveIndicatorView.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        connectedLabel.text = "Connecting..."//TODO: move to localizable.strings
        blurView.isHidden = false
        
        seeOlder.isHidden = true
        seeMoreRecent.isHidden = true
        
        UIView.animate(withDuration: 1, delay: 0.0, options: [.curveEaseInOut, .repeat, .autoreverse],
            animations: {
                self.showLiveIndicatorView.alpha = Constants.UIValues.alphaLabel
        }, completion: nil)

        presenter?.getSavedData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter?.saveData()
    }
    
    private func setupRightCornersRadii(view: UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = Constants.UIValues.cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    @IBAction func seeOlder(_ sender: Any) {
        fetchOlder()
    }
    
    @IBAction func seeMoreRecent(_ sender: Any) {
        fetchMoreRecent()
    }
    
    private func fetchOlder() {
        if currentIndex > 0 {
            currentIndex = currentIndex - 1
            presenter?.getDataForIndex(index: currentIndex)
        }
    }
    
    private func fetchMoreRecent() {
        if currentIndex < (flowDataSize - 1) {
            currentIndex = currentIndex + 1
            presenter?.getDataForIndex(index: currentIndex)
        }
    }
    
    @objc func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: swipeView)

        let threeshold = 50 as CGFloat
        if panGesture.state == .began {
            oldPanOffset = translation.x
            timeIndicatorHeight.constant = Constants.UIValues.timeIndicatorHeight
            view.layoutIfNeeded()
        }
        
        if panGesture.state == .changed {
            if  (translation.x - oldPanOffset) > threeshold {
                oldPanOffset = translation.x
                fetchOlder()
            } else if (translation.x - oldPanOffset) < -threeshold {
                oldPanOffset = translation.x
                fetchMoreRecent()
            }
        }
        
        if panGesture.state == .ended {
            timeIndicatorHeight.constant = 8
            view.layoutIfNeeded()
        }
    }
}


extension HomeViewController: HomeDelegate {
    func flowDidConnect() {
        connectedLabel.text = "Connected"//TODO: move to localizable.strings
    }
    
    func flowDidDisconnect() {
        connectedLabel.text = "Disconnected"//TODO: move to localizable.strings
        blurView.isHidden = false
    }
    
    func flowDidFetch(dataSize: Int) {
        flowDataSize = dataSize

        if showLive && dataSize > 0 {
            currentIndex = dataSize - 1
        }
        presenter?.getDataForIndex(index: currentIndex)
    }
    
    func flowDidFetchOldData(oldDataSize: Int) {
        currentIndex = (oldDataSize - 1) + currentIndex
        flowDataSize = flowDataSize + oldDataSize
    }

    
    func updateViewWithData(data: FlowModel, dataSize: Int) {
        blurView.isHidden = true
        
        dateLabel.text = data.getDateString()
        
        vocLabel.text = " VOC: \(data.voc) "
        vocWidthConstraint.constant = (CGFloat(data.voc) * UIScreen.main.bounds.width ) / CGFloat(data.getMaxValue())
        vocBarView.backgroundColor = data.getColorFrom(pollutant: data.voc)
        
        no2Label.text = " NO2: \(data.no2) "
        no2WidthConstraint.constant = (CGFloat(data.no2) * UIScreen.main.bounds.width ) / CGFloat(data.getMaxValue())
        no2BarView.backgroundColor = data.getColorFrom(pollutant: data.no2)

        
        pm10Label.text = " PM10: \(data.pm10) "
        pm10WidthConstraint.constant = (CGFloat(data.pm10) * UIScreen.main.bounds.width ) / CGFloat(data.getMaxValue())
        pm10BarView.backgroundColor = data.getColorFrom(pollutant: data.pm10)

        pm25Label.text = " PM25: \(data.pm25) "
        pm25WidthConstraint.constant = (CGFloat(data.pm25) * UIScreen.main.bounds.width ) / CGFloat(data.getMaxValue())
        pm25BarView.backgroundColor = data.getColorFrom(pollutant: data.pm25)
        
        
        
        seeOlder.isHidden = currentIndex == 0 || dataSize == 1
        seeMoreRecent.isHidden = (currentIndex == dataSize - 1) || dataSize == 1
        
        showLive = (currentIndex == dataSize - 1)
        showLiveLabel.isHidden = !showLive
        showLiveIndicatorView.isHidden = !showLive

        
        if dataSize <= 1 || currentIndex == dataSize - 1 {
            timeIndicatorRightMargin.constant = 0
        } else if dataSize > 1 {
            timeIndicatorRightMargin.constant = ((CGFloat((dataSize - 1) - currentIndex) * UIScreen.main.bounds.width ) / CGFloat(dataSize - 1) ) - timeIndicator.frame.width
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}
