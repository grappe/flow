//
//  HomePresenter.swift
//  flow-mvp
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import Foundation
import Bluetooth

protocol HomeDelegate {
    func flowDidConnect()
    func flowDidDisconnect()
    func flowDidFetch(dataSize: Int)
    func updateViewWithData(data: FlowModel, dataSize: Int)
    func flowDidFetchOldData(oldDataSize: Int)
}

class HomePresenter {
    var delegate: HomeDelegate
    var bleManager: BluetoothManager?
    var data = [FlowModel]()
    
    init(delegate: HomeDelegate) {
        self.delegate = delegate
        bleManager = BluetoothManager(delegate: self)
    }
    
    func getDataForIndex(index: Int) {
        if index >= 0 && index < data.count {
            delegate.updateViewWithData(data: data[index], dataSize: data.count)
        }
    }
    
    func saveData() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(data) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: Constants.Database.databaseKey)
        }
    }
    
    func getSavedData() {
        let defaults = UserDefaults.standard
        if let loadedDataObject = defaults.object(forKey: Constants.Database.databaseKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedData = try? decoder.decode(Array<FlowModel>.self, from: loadedDataObject) {
                data.insert(contentsOf: loadedData, at: 0)
                delegate.flowDidFetchOldData(oldDataSize: loadedData.count)
            }
        }
    }
    
    private func groupNewData(newData: [Measure]) {
        print("groupNewData \(newData)")
        for measure in newData {
            if let dataFoundIndex = data.firstIndex(where: { $0.timestamp == measure.timestamp }) {
                var dataFound = data[dataFoundIndex]
                switch measure.type {
                case .voc:
                    dataFound.voc = measure.aqi
                case .no2:
                    dataFound.no2 = measure.aqi
                case .pm10:
                    dataFound.pm10 = measure.aqi
                case .pm25:
                    dataFound.pm25 = measure.aqi
                }
                
                data[dataFoundIndex] = dataFound
                
            } else {
                var newData: FlowModel
                switch measure.type {
                case .voc:
                    newData = FlowModel(timestamp: measure.timestamp, voc: measure.aqi, no2: 0.0, pm10: 0.0, pm25: 0.0)
                case .no2:
                    newData = FlowModel(timestamp: measure.timestamp, voc: 0.0, no2: measure.aqi, pm10: 0.0, pm25: 0.0)
                case .pm10:
                    newData = FlowModel(timestamp: measure.timestamp, voc: 0.0, no2: 0.0, pm10: measure.aqi, pm25: 0.0)
                case .pm25:
                    newData = FlowModel(timestamp: measure.timestamp, voc: 0.0, no2: 0.0, pm10: 0.0, pm25: measure.aqi)
                }
                
                data.append(newData)
            }
        }
    }
}

extension HomePresenter: BluetoothManagerDelegate {
    func flowDidConnect() {
        delegate.flowDidConnect()
    }
    
    func flowDidDisconnect() {
        delegate.flowDidDisconnect()
    }
    
    func flowDidFetch(newData: [Measure]) {
        groupNewData(newData: newData)
        delegate.flowDidFetch(dataSize: data.count)
    }
}
