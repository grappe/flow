//
//  Bluetooth.h
//  Bluetooth
//
//  Created by Vincent Lemonnier on 06/07/2020.
//  Copyright © 2020 Plume Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Bluetooth.
FOUNDATION_EXPORT double BluetoothVersionNumber;

//! Project version string for Bluetooth.
FOUNDATION_EXPORT const unsigned char BluetoothVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Bluetooth/PublicHeader.h>


