//
//  BluetoothManager.swift
//  flow-mvp
//
//  Created by grAppe on 10/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import Foundation
import Bluetooth
import CoreBluetooth


protocol BluetoothManagerDelegate {
    func flowDidConnect()
    func flowDidDisconnect()
    func flowDidFetch(newData: [Measure])
}

class BluetoothManager: NSObject {
    
    var delegate: BluetoothManagerDelegate
    private let bleClient = mockClient()
    private var centralManager = CBCentralManager()
    private var data = [Measure]()
    
    init(delegate: BluetoothManagerDelegate) {
        self.delegate = delegate
        super.init()
        centralManager.delegate = self
    }
    
    private func connectToFlow() {
        bleClient.connect(onComplete: { (client, error) in
            if error != nil {
                self.delegate.flowDidConnect()
                self.syncToFlow()
            } else {
//                print("bleClient error \(error.debugDescription)")
                self.delegate.flowDidDisconnect()
                self.connectToFlow()
            }
        })
    }
    
    private func syncToFlow() {
        self.bleClient.sync(onData: { (client, measures) in
            self.data = measures
            self.delegate.flowDidFetch(newData: measures)
        },
        onFailure: { (client, error) in
//            print("sync onFailure \(error)")
            switch error {
            case .notConnected:
                self.connectToFlow()
            default:
                self.syncToFlow()
            }
        })
    }
}



extension BluetoothManager: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state != .poweredOn {
            print("Central is not powered on")
        } else {
            print("bleClient \(bleClient.deviceName)")
            connectToFlow()
        }
    }
}
