//
//  Constants.swift
//  flow-mvp
//
//  Created by grAppe on 12/07/2020.
//  Copyright © 2020 grAppe. All rights reserved.
//

import UIKit

struct Constants {
    struct UIValues {
        static let timeIndicatorHeight = 20.0 as CGFloat
        static let cornerRadius = 5.0 as CGFloat
        static let alphaLabel = 0.3 as CGFloat
        static let whiteLabel = 0.5 as CGFloat

    }
    
    
    struct Database {
        static let databaseKey = "saveDataKey"
    }
}
